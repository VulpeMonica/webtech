
const FIRST_NAME = "Vulpe";
const LAST_NAME = "Monica";
const GRUPA = "1085";

function numberParser(value) {
    if(value>Number.MAX_SAFE_INTEGER || value<Number.MIN_SAFE_INTEGER)
    return NaN;
    else
    return Number.parseInt(value);
}


/**
function numberParser(value) {
    if (value % 1 === 0) {
        if (value > Number.MAX_SAFE_INTEGER || value < Number.MIN_SAFE_INTEGER) {
            return NaN
        }
        else
            return value
    }
    if (value % 1 !== 0) {
        return Number.parseInt(value)
    }
    if (typeof value === "string") {
        return Number.parseInt(value)
    }
    if (isNaN(value) || NaN !== NaN)
        return NaN
    else
        return Number.parseInt(value)
    if (value === Infinity || value === -Infinity) {
        return NaN
    }
}
*/



module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

