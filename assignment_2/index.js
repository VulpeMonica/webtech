
const FIRST_NAME = "Vulpe";
const LAST_NAME = "Monica";
const GRUPA = "1085";

/**
 * Make the implementation here
 */
function initCaching() {
   var cache = {
    pageAccessCounter: function(key="home"){
        key = key.toLowerCase()
        if(this[key]==undefined){
            this[key]=1;
        }
        else
        {
            this[key]++;
        }
            
    },
    getCache: function(){
        return this;
    },
    }

    return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

