
const FIRST_NAME = "VULPE";
const LAST_NAME = "MONICA-IOANA";
const GRUPA = "1085";


class Employee {
    constructor(_name,_surname,_salary){
        this.name=_name;
        this.surname=_surname;
        this.salary=_salary;
    }

    // set name(value) {
    //     this._name = value;
    // }

    // get name(){
    //     return this._name;
    // }

    // set surname(value) {
    //     this._surname = surname;
    // }
    // get surname() {
    //     return this._surname;
    // }

    // get salary(){
    //     return this._salary;
    // }

    // set salary(value){
    //     this._salary=value;
    // }

    getDetails(){
        return this.name + " " + this.surname + " " + this.salary;
    }
}

class SoftwareEngineer extends Employee {
   constructor(name,surname,salary,experience){
       super(name,surname,salary);
       if(experience === undefined)
            this.experience="JUNIOR";
        else
            this.experience=experience;
   }
   applyBonus(){
       if(this.experience === "SENIOR")
            return this.salary + this.salary * 0.2;
          else
            if(this.experience === "MIDDLE")
                return this.salary + this.salary * 0.15;
              else
                return this.salary + this.salary * 0.1;  

   }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

